/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 * Track open windows, and close the program when they are all closed.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class WindowCloseManager {

	public static final WindowClosingListener HIDE_ON_CLOSE = (f, n) -> f.setVisible(false);
	public static final WindowClosingListener DISPOSE_ON_CLOSE = (f, n) -> f.dispose();
	public static final WindowClosingListener DO_NOTHING_ON_CLOSE = (f, n) -> {
	};

	/**
	 * Track the number of open frames.
	 */
	private int counter;

	/**
	 * Start tracking a frame.
	 *
	 * @param frame The frame to track.
	 * @param listener What to do when the close button is pressed.
	 */
	public void trackFrame(JFrame frame, WindowClosingListener listener) {
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				WindowCloseManager.this.windowClosed(e);
			}

			@Override
			public void windowClosing(WindowEvent e) {
				listener.closePressed(frame, getOpenFrames());
			}
		});
		counter++;
	}

	/**
	 * Get the number of open frames.
	 *
	 * @return The number of open frames.
	 */
	public int getOpenFrames() {
		return counter;
	}

	private void windowClosed(WindowEvent e) {
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			counter--;
			if (counter <= 0) {
				System.exit(0);
			}
		}
	}

	public interface WindowClosingListener {

		/**
		 * The window is trying to close.
		 *
		 * @param frame The frame that is closing.
		 * @param openFrames The number of frames currently open (including this
		 * one).
		 */
		void closePressed(JFrame frame, int openFrames);
	}

}
