/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.gui;

import javax.swing.table.AbstractTableModel;
import xyz.znix.cfdl.api.json.AddonFile;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class VersionsTableModel extends AbstractTableModel {

	private static final String[] NAMES = {
		"Release Date",
		"File Name",
		"File Type",
		"Download"
	};

	private final AddonFileSupplier fileProvider;

	public VersionsTableModel(AddonFileSupplier fileProvider) {
		this.fileProvider = fileProvider;
	}

	@Override
	public int getRowCount() {
		return fileProvider.getCount();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		return NAMES[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		AddonFile file = fileProvider.get(rowIndex);
		switch (columnIndex) {
			case 0:
				return file.FileDate;
			case 1:
				return file.FileName;
			case 2:
				return file.ReleaseType;
			case 3:
				return "";
			default:
				throw new IllegalArgumentException("Bad rowIndex " + rowIndex);
		}
	}

	public interface AddonFileSupplier {

		AddonFile get(int rowIndex);

		int getCount();
	}
}
