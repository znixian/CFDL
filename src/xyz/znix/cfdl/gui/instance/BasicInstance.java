/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.gui.instance;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import xyz.znix.cfdl.api.CFAPI;
import xyz.znix.cfdl.api.json.Addon;
import xyz.znix.cfdl.api.json.AddonFile;
import xyz.znix.cfdl.api.json.GameFile;
import xyz.znix.cfdl.gui.InstancesPanel;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class BasicInstance implements IInstance {

	private String name;
	private File baseDir;
	private transient List<AddonFile> installedAddons;

	private transient CFAPI api;
	private transient InstancesPanel.InstanceIO io;

	/**
	 * For GSON. Do not use.
	 */
	private BasicInstance() {
		installedAddons = new ArrayList<>();
	}

	public BasicInstance(String name, File baseDir, CFAPI api, InstancesPanel.InstanceIO io) {
		if (api == null) {
			throw new IllegalStateException("API Cannot be null!");
		}
		this.api = api;
		this.name = name;
		this.baseDir = baseDir;
		this.io = io;

		installedAddons = new ArrayList<>();
	}

	public void loadApi(CFAPI api, InstancesPanel.InstanceIO io) {
		if (api == null) {
			throw new IllegalStateException("API Cannot be null!");
		}
		if (io == null) {
			throw new IllegalStateException("IO Cannot be null!");
		}
		if (this.api != null) {
			throw new IllegalStateException("Cannot load API twice!");
		}

		this.api = api;
		this.io = io;
	}

	@Override
	public File getTargetDirectory(AddonFile addon) {
		String offset = addon.addon.CategorySection.Path;
		return new File(baseDir, offset);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public File getManifestFile() {
		return new File(baseDir, "CFDL-MANIFEST");
	}

	@Override
	public void installLatest(Addon addon) {
		GameFile target = addon.GameVersionLatestFiles.stream()
				.filter(f -> isMcVersionCompadible(f.GameVesion))
				.sorted((a, b) -> b.ProjectFileID - a.ProjectFileID)
				.findFirst()
				.orElseThrow(() -> new RuntimeException("No matching file?"));

		AddonFile file = api.getAddonFileForGameFile(target);
		installAddonFile(file);
	}

	@Override
	public void installAddonFile(AddonFile addon) {
		api.cacheAddonFile(addon);
		installedAddons.add(addon);
		io.save();
		updateFiles();
	}

	private void updateFiles() {
		try {
			for (AddonFile file : installedAddons) {
				File cached = api.getAddonFileLocation(file);
				File targetDir = getTargetDirectory(file);
				File targetFile = new File(targetDir, cached.getName());

				if (targetFile.exists()) {
					continue;
				}
				if (api.getIo().canUseSymbolicLinks()) {
					Files.createSymbolicLink(targetFile.toPath(), cached.getAbsoluteFile().toPath());
				} else {
					Files.copy(targetFile.toPath(), cached.toPath());
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(BasicInstance.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void removeAddonFile(AddonFile addon) {
		File cached = api.getAddonFileLocation(addon);
		File targetDir = getTargetDirectory(addon);
		File targetFile = new File(targetDir, cached.getName());
		targetFile.delete();

		installedAddons.remove(addon);
		io.save();
		updateFiles();
	}

	@Override
	public boolean isMcVersionCompadible(String version) {
		// TODO write some logic for this.
		return version.equals("1.10.2");
	}

}
