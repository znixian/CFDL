/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.gui.instance;

import java.io.File;
import xyz.znix.cfdl.api.json.Addon;
import xyz.znix.cfdl.api.json.AddonFile;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public interface IInstance {

	File getTargetDirectory(AddonFile addon);

	File getManifestFile();

	default String getName() {
		return toString();
	}

	void installLatest(Addon addon);

	void installAddonFile(AddonFile addon);

	void removeAddonFile(AddonFile addon);

	boolean isMcVersionCompadible(String version);
}
