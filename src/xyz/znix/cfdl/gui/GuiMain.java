/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.gui;

import java.awt.CardLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import xyz.znix.cfdl.api.CFAPI;
import xyz.znix.cfdl.api.json.Addon;
import xyz.znix.cfdl.gui.instance.IInstance;

/**
 * The GUI wrapper class. Opens the main window, and adds in tabs.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class GuiMain {

	private final JFrame frame;
	private final WindowCloseManager closeManager;
	private final CFAPI curse;

	private final CardLayout cards;
	private final InstancesPanel instances;
	private final AddonBrowserPanel addons;
	private final AddonDetails details;

	public GuiMain() {
		closeManager = new WindowCloseManager();

		curse = new CFAPI();
		curse.init(); // TODO move to a seperate thread.

		instances = new InstancesPanel(curse, this);
		addons = new AddonBrowserPanel(curse, this);
		details = new AddonDetails(curse, this);

		JPanel contentPane = new JPanel();
		cards = new CardLayout();
		contentPane.setLayout(cards);
		contentPane.add(instances, "instances");
		contentPane.add(addons, "addons");
		contentPane.add(details, "details");

		if (!curse.isAuthenticated()) {
			addLoginPanel(contentPane, cards);
		}

		frame = new JFrame("CFDL");
		frame.setContentPane(contentPane);
		frame.setBounds(100, 100, 500, 500);
		closeManager.trackFrame(frame, (f, openFrames) -> {
			if (openFrames == 1) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
	}

	private void addLoginPanel(JPanel contentPane, CardLayout cards) {
		contentPane.add(new LoginPanel((user, pass, panel) -> {
			panel.setStatus(LoginPanel.Status.WORKING);
			new Thread(() -> {
				boolean succeeded = curse.login(user, pass);
				panel.setStatus(succeeded ? LoginPanel.Status.NONE : LoginPanel.Status.INVALID);

				if (succeeded) {
					showInstancesPanel();
					contentPane.remove(panel);
				}
			}).start();
		}), "login");
		cards.show(contentPane, "login");
	}

	public void showInstancesPanel() {
		cards.show(frame.getContentPane(), "instances");
	}

	public void showModsScreen(IInstance instance) {
		addons.switchTo(instance);
		cards.show(frame.getContentPane(), "addons");
	}

	public void showAddonDetails(IInstance instance, Addon addon) {
		details.show(addon, instance);
		cards.show(frame.getContentPane(), "details");
	}
}
