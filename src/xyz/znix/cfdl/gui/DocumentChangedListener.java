/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.gui;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class DocumentChangedListener implements DocumentListener {

	private final Runnable target;

	public DocumentChangedListener(Runnable target) {
		this.target = target;
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		target.run();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		target.run();
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		// doesn't occur from normal stuff
	}
}
