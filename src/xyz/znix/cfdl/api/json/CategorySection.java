/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class CategorySection {

	public int ID;
	public int GameID;
	public String Name;
	public String PackageType;
	public String Path;
	public String InitialInclusionPattern;
	public String ExtraIncludePattern;
}
