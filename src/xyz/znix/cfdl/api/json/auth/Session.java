/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json.auth;

import java.io.Serializable;

/**
 * Represents a session the user has open with the curse servers
 *
 * @author znix
 */
public class Session implements Serializable {

	public String actual_premium_status;
	public String effective_premium_status;
	public String email_address;
	public String session_id;
	public String subscription_token;
	public String token;
	public String user_id;
	public String username;

	public String getAuthToken() {
		return "Token " + user_id + ":" + token;
	}
}
