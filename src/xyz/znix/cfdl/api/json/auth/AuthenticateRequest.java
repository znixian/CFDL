/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json.auth;

/**
 *
 * @author znix
 */
public class AuthenticateRequest {
	private String username;
	private String password;

	public AuthenticateRequest() {
	}

	public AuthenticateRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}
}
