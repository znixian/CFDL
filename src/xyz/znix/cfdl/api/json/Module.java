/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Module {

	@SerializedName(value = "Foldername", alternate = "foldername")
	public String Foldername;
	@SerializedName(value = "Fingerprint", alternate = "fingerprint")
	public long Fingerprint;
}
