/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Attachment {

	public String Description;
	public boolean IsDefault;
	public String ThumbnailUrl;
	public String Title;
	public String Url;
}
