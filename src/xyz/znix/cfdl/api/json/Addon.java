/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json;

import java.util.List;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Addon {

	public int Id;
	public String Name;
	public List<Author> Authors;
	public Attachment[] Attachments;
	public String WebSiteURL;
	public int GameId;
	public String Summary;
	public int DefaultFileId;
	public int CommentCount;
	public int DownloadCount;
	public int Rating;
	public int InstallCount;
	public int IconId;
	public AddonFile[] LatestFiles;
	public Category[] Categories;
	public String PrimaryAuthorName;
	public String ExternalUrl;
	public String Status;
	public String Stage;
	public String DonationUrl;
	public int PrimaryCategoryId;
	public String PrimaryCategoryName;
	public String PrimaryCategoryAvatarUrl;
	public int Likes;
	public CategorySection CategorySection;
	public String PackageType;
	public String AvatarUrl;
	public List<GameFile> GameVersionLatestFiles;
	public int IsFeatured;
	public double PopularityScore;
	public int GamePopularityRank;
}
