/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Dependency {

	@SerializedName(value = "AddOnId", alternate = "add_on_id")
	public int AddOnId;
	@SerializedName(value = "Type", alternate = "type")
	private String TypeCode;

	public Type getType() {
		switch (TypeCode) {
			case "2":
			case "Optional":
				return Type.Optional;
			case "1":
			case "3":
			case "Required":
				return Type.Required;
			default:
				// unknown, assume required
				System.out.println("Unknown type " + TypeCode);
				return Type.Required;
		}
	}

	public enum Type {
		Required,
		Optional;
	}
}
