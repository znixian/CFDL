/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api.json;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class AddonFile {

	public transient Addon addon;

	@SerializedName(value = "Id", alternate = "id")
	public int Id;
	@SerializedName(value = "FileName", alternate = "file_name")
	public String FileName;
	@SerializedName(value = "FileNameOnDisk", alternate = "file_name_on_disk")
	public String FileNameOnDisk;
	@SerializedName(value = "FileDate", alternate = "file_date")
	public String FileDate;
	@SerializedName(value = "ReleaseType", alternate = "release_type")
	public String ReleaseType;
	@SerializedName(value = "FileStatus", alternate = "file_status")
	public String FileStatus;
	@SerializedName(value = "DownloadURL", alternate = "download_url")
	public String DownloadURL;
	@SerializedName(value = "IsAlternate", alternate = "is_alternate")
	public boolean IsAlternate;
	@SerializedName(value = "AlternateFileId", alternate = "alternate_file_id")
	public int AlternateFileId;
	@SerializedName(value = "Dependencies", alternate = "dependencies")
	public Dependency[] Dependencies;
	@SerializedName(value = "IsAvailable", alternate = "is_available")
	public boolean IsAvailable;
	@SerializedName(value = "Modules", alternate = "modules")
	public Module[] Modules;
	@SerializedName(value = "PackageFingerprint", alternate = "package_fingerprint")
	public long PackageFingerprint;
	@SerializedName(value = "GameVersion", alternate = "game_version")
	public String[] GameVersion;
}
