/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import xyz.znix.cfdl.api.json.Addon;
import xyz.znix.cfdl.api.json.AddonFile;
import xyz.znix.cfdl.api.json.Feed;
import xyz.znix.cfdl.api.json.GameFile;
import xyz.znix.cfdl.api.json.auth.AuthResponce;
import xyz.znix.cfdl.api.json.auth.AuthenticateRequest;
import xyz.znix.cfdl.api.json.auth.Session;
import xyz.znix.cfdl.api.json.misc.Description;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class CFAPI {

	private static final File DATA_DIR;
	private static final File CACHE_DIR;
	private static final File FEED_COMPLETE_BZ;
	private static final File FEED_COMPLETE_JSON;
	private static final File AUTH_FILE;
	public static final File ICON_CACHE_DIR;
	private static final File ADDDON_META_CACHE_DIR;
	public static final File INSTANCE_SPEC;
	private static final File ADDDON_CONTENT_DIR;

	static {
		DATA_DIR = check(new File("data"));
		CACHE_DIR = check(new File(DATA_DIR, "cache"));
		ADDDON_META_CACHE_DIR = check(new File(CACHE_DIR, "addon_meta"));
		ADDDON_CONTENT_DIR = check(new File(CACHE_DIR, "addon_content"));

		FEED_COMPLETE_BZ = new File(CACHE_DIR, "feed-complete.json.bz2");
		FEED_COMPLETE_JSON = new File(CACHE_DIR, "feed-complete.json");
		AUTH_FILE = new File(DATA_DIR, "authkey");

		ICON_CACHE_DIR = check(new File(CACHE_DIR, "icon"));

		INSTANCE_SPEC = new File(DATA_DIR, "instances");
	}

	private static File check(File file) {
		if (!file.exists()) {
			file.mkdir();
		}
		return file;
	}

	private final Map<Integer, String> categoryIconUrls;
	private final FetchAPI io;
	private final Gson gson;
	private Feed feed;
	private Session session;

	public CFAPI() {
		categoryIconUrls = new HashMap<>();
		gson = new GsonBuilder().create();
		io = new FetchAPI();

		if (AUTH_FILE.exists()) {
			session = io.readJson(gson, AUTH_FILE, Session.class);
		}
	}

	/**
	 * Download the feed, etc. This may take a while, so don't run it on the AWT
	 * thread.
	 */
	public void init() {
		if (feed != null) {
			throw new IllegalStateException("Cannot Init Twice!");
		}

		try {
			// make sure our feed is downloaded
			if (!FEED_COMPLETE_BZ.exists()) {
				io.downloadFileTo(IOSTAT.FEED_COMPLETE_URL, FEED_COMPLETE_BZ);
			}

			if (!FEED_COMPLETE_JSON.exists()) {
				// decompress and parse our stream. This might be a tad slow...
				try (BZip2CompressorInputStream in
						= new BZip2CompressorInputStream(
								new BufferedInputStream(
										new FileInputStream(FEED_COMPLETE_BZ)
								)
						)) {
					Files.copy(in, FEED_COMPLETE_JSON.toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
			}

			try (InputStreamReader reader = new InputStreamReader(
					new BufferedInputStream(
							new FileInputStream(FEED_COMPLETE_JSON)
					)
			)) {
				feed = gson.fromJson(reader, Feed.class);
			}

			// find all the category icons
			feed.data.stream()
					.filter(a -> !(categoryIconUrls.containsKey(a.PrimaryCategoryId)))
					.forEach(a -> categoryIconUrls.put(a.PrimaryCategoryId, a.PrimaryCategoryAvatarUrl));

			for (Addon addon : feed.data) {
				for (GameFile file : addon.GameVersionLatestFiles) {
					file.addon = addon;
				}
				for (AddonFile file : addon.LatestFiles) {
					file.addon = addon;
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(CFAPI.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Try logging in to curse.
	 *
	 * @param user The username
	 * @param pass The password
	 * @throws java.lang.IllegalStateException If the API is already logged in.
	 * @return {@code true} if the login was successful, {@code false}
	 * otherwise.
	 */
	public boolean login(String user, String pass) {
		try {
			System.out.println("Logging in...");

			AuthenticateRequest req = new AuthenticateRequest(user, pass);

			// make the HTTPS request
			String result = io.sendPostRequest(IOSTAT.API_AUTH, null, gson.toJson(req));

			// returns null for resp. codes other than 200 OK
			if (result == null) {
				return false;
			}

			// load the session from JSON
			session = gson.fromJson(result, AuthResponce.class).session;

			// save the session
			io.saveJson(gson, AUTH_FILE, session);

			return true;
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	// request methods
	public void getDescription(Addon addon, Consumer<Description> callback) {
		simpleApiReq(addon, "description", Description.class, callback);
	}

	public void getFiles(Addon addon, Consumer<xyz.znix.cfdl.api.json.misc.Files> callback) {
		Consumer<xyz.znix.cfdl.api.json.misc.Files> action = files -> {
			if (files == null || files.files == null) {
				return;
			}
			files.files.stream().forEach(file -> file.addon = addon);
		};

		simpleApiReq(addon, "files", xyz.znix.cfdl.api.json.misc.Files.class, action.andThen(callback));
	}

	private <T> void simpleApiReq(Addon addon, String name, Class<T> type, Consumer<T> callback) {
		// find a place for the cache file
		File addonDir = new File(ADDDON_META_CACHE_DIR, "addon_" + addon.Id);
		addonDir.mkdir();
		File cacheFile = new File(addonDir, name);

		// build the URL
		String url = IOSTAT.API_ADDON + addon.Id + "/" + name;

		T cached = io.getCachedApiCall(gson, cacheFile, type, FetchAPI.DEFAULT_CACHE_TIME);
		if (cached != null) {
			callback.accept(cached);
			return;
		}

		// send off the request, on a different thread
		io.runNetworkTask(() -> io.apiCall(session, gson, url, cacheFile, type), callback);
	}

	// getters
	public Feed getFeed() {
		return feed;
	}

	public boolean isAuthenticated() {
		return session != null;
	}

	public FetchAPI getIo() {
		return io;
	}

	public AddonFile getAddonFileForGameFile(GameFile target) {
		Addon addon = target.addon;

		// see if it in the latest files.
		for (AddonFile file : addon.LatestFiles) {
			if (file.Id == target.ProjectFileID) {
				return file;
			}
		}

		// find a place for the cache file
		File addonDir = new File(ADDDON_META_CACHE_DIR, "addon_" + addon.Id);
		File filemeta = new File(addonDir, "filemeta");
		filemeta.mkdirs();

		File cacheFile = new File(filemeta, target.GameVesion + "-" + target.ProjectFileID);

		// build the URL
		String url = IOSTAT.API_ADDON + addon.Id + "/file/" + target.ProjectFileID;

		AddonFile cached = io.getCachedApiCall(gson, cacheFile, AddonFile.class, -1);
		if (cached != null) {
			// setup the addon variable
			cached.addon = addon;

			return cached;
		}

		// TODO possibly run using io.runNetworkTask
		// Note: using -1 for the cache time makes it not expire, ever. This makes
		//  sence, as this file should never change.
		AddonFile file = io.apiCall(session, gson, url, cacheFile, AddonFile.class, -1);

		// setup the addon variable
		file.addon = addon;

		System.out.println(file.addon);

		return file;
	}

	public File getAddonFileLocation(AddonFile file) {
		return getAddonFileLocation(file, "");
	}

	private File getAddonFileLocation(AddonFile file, String extension) {
		Addon addon = file.addon;
		File addonDir = new File(ADDDON_CONTENT_DIR, "addon_" + addon.Id);
		File fileDir = new File(addonDir, "" + file.GameVersion[0] + "-" + file.Id);
		fileDir.mkdirs();

		return new File(fileDir, file.FileNameOnDisk + extension);
	}

	public boolean isAddonFileCached(AddonFile file) {
		return getAddonFileLocation(file).exists();
	}

	public void cacheAddonFile(AddonFile file) {
		if (isAddonFileCached(file)) {
			return;
		}

		// download to myfile.jar.dl, then rename it to myfile.jar
		//  this stops it from being used as a cached version
		//  should the download be interrupted.
		File downloadTemp = getAddonFileLocation(file, ".dl");
		File primary = getAddonFileLocation(file);
		try {
			System.out.println("Caching file " + file.FileName);
			io.downloadFileTo(file.DownloadURL, downloadTemp);

			downloadTemp.renameTo(primary);
		} catch (IOException ex) {
			Logger.getLogger(CFAPI.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
