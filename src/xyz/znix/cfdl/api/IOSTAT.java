/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class IOSTAT {

	private static final String FEED_BASE_URL = "http://clientupdate-v6.cursecdn.com/feed/addons/432/v10/";
	public static final String FEED_COMPLETE_URL = FEED_BASE_URL + "complete.json.bz2";
	public static final String FEED_HOURLY_URL = FEED_BASE_URL + "hourly.json.bz2";
	public static final String API_BASE = "https://curse-rest-proxy.azurewebsites.net/api/";
	public static final String API_AUTH = API_BASE + "authenticate";
	public static final String API_ADDON = API_BASE + "addon/";

	private IOSTAT() {
	}
}
