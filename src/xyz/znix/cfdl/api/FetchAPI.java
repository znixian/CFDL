/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl.api;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;
import javax.swing.SwingUtilities;
import xyz.znix.cfdl.api.json.auth.Session;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class FetchAPI {

	public static final int DEFAULT_CACHE_TIME = 24 * 60 * 60 * 1000; // 24H

	private static final Map<String, String> CONTENT_TYPE_JSON;

	static {
		Map<String, String> _CONTENT_TYPE_JSON = new HashMap<>();
		_CONTENT_TYPE_JSON.put("Content-Type", "application/json");

		CONTENT_TYPE_JSON = Collections.unmodifiableMap(_CONTENT_TYPE_JSON);
	}

	private final Gson gson;

	public FetchAPI() {
		gson = new Gson();
	}

	public <T> T readJson(Gson gson, File file, Class<T> type) {
		try {
			// read the json file to an array of lines
			String json = Files.lines(file.toPath())
					// concatenate those lines together.
					.reduce("", (a, b) -> a + "\n" + b);

			// parse the JSON, and get the file name.
			return gson.fromJson(json, type);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public void saveJson(Gson gson, File file, Object data) {
		try {
			String json = gson.toJson(data);
			Files.write(Paths.get(file.toURI()), json.getBytes("utf-8"),
					StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public <T> T apiCall(Session session, Gson gson, String url, File cacheFile, Class<T> type) {
		return apiCall(session, gson, url, cacheFile, type, DEFAULT_CACHE_TIME);
	}

	public <T> T getCachedApiCall(Gson gson, File cacheFile, Class<T> type, int cacheTime) {
		long time = System.currentTimeMillis();

		// if the cache file exists, parse it and check that is is up to date.
		if (cacheFile.exists()) {
			CachedApiCall data = readJson(this.gson, cacheFile, CachedApiCall.class);
			if (cacheTime == -1 || data.downloadTimestamp + cacheTime > time) { //  && data.contents != null
				// parse the contents, and hand that off.
				return gson.fromJson(data.contents, type);
			}
		}

		return null;
	}

	/**
	 * Runs a API call, and returns the un-json'ed version of it.
	 *
	 * @param <T> The type to parse into.
	 * @param session The session to use for authentication.
	 * @param gson The GSON object to use for parsing
	 * @param url The URL to fetch from.
	 * @param cacheFile The file to use for caching.
	 * @param type The type to parse into.
	 * @param cacheTime The number of milliseconds this cache is valid for.
	 * @return The object served on this API endpoint.
	 */
	public <T> T apiCall(Session session, Gson gson, String url,
			File cacheFile, Class<T> type, int cacheTime) {

		T cached = getCachedApiCall(gson, cacheFile, type, cacheTime);
		if (cached != null) {
			return cached;
		}

		System.out.println("Calling API endpoint " + url);

		// build our header, including our access token.
		Map<String, String> header
				= Collections.singletonMap("Authorization", session.getAuthToken());

		// send off a GET request
		String result;
		try {
			result = sendGetRequest(url, header);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		// cache the result
		long time = System.currentTimeMillis();
		CachedApiCall cache = new CachedApiCall(time, result);
		saveJson(this.gson, cacheFile, cache);

		// parse and return the result
		return gson.fromJson(result, type);
	}

	public File fetch(String textUrl, File cache) {
		try {
			return fetch(textUrl, cache, DEFAULT_CACHE_TIME);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public File fetch(String textUrl, File cache, int cacheTime) throws IOException {
		// TODO make cacheTime work

		// if we have cached the file, use it!
		if (cache.exists()) {
			CacheData data = readJson(gson, cache, CacheData.class);
			File downloadedFile = new File(cache.getParentFile(), data.fileName);
			return downloadedFile;
		} else {
//			System.out.println("Downloading " + textUrl);
			// add the extension .dl onto the cache path
			File target = new File(cache.getParentFile(), cache.getName() + ".dl");

			// actually download it
			downloadFileTo(textUrl, target);

			// write up the cache file, so we don't have to download it next time.
			CacheData data = new CacheData(System.currentTimeMillis(), target.getName());
			saveJson(gson, cache, data);
			return target;
		}
	}

	public void downloadFileTo(String textUrl, File target) throws IOException {
		URL url = new URL(textUrl);
		ReadableByteChannel rbc = Channels.newChannel(url.openStream());
		FileOutputStream fos = new FileOutputStream(target);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
	}

	public String sendGetRequest(String url, Map<String, String> headers) throws IOException {
		return sendRequest(url, "GET", headers, null);
	}

	public String sendPostRequest(String url, Map<String, String> headers, String data) throws IOException {
		return sendRequest(url, "POST", CONTENT_TYPE_JSON, data);
	}

	private String sendRequest(String url, String method,
			Map<String, String> headers, String sendData) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod(method);

		// add request headers
		if (headers != null) {
			headers.entrySet().stream().forEach((entry) -> {
				con.setRequestProperty(entry.getKey(), entry.getValue());
			});
		}

		if (sendData != null) {
			con.setDoOutput(true);
			try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
				wr.writeBytes(sendData);
				wr.flush();
			}
		}

		int responseCode = con.getResponseCode();
		System.out.println(responseCode);
		if (responseCode != 200) {
			return null;
		}

		StringBuilder response;
		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()))) {
			String inputLine;
			response = new StringBuilder();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine).append('\n');
			}
		}

		return response.toString();
	}

	public <T> void runNetworkTask(Supplier<T> networkTask, Consumer<T> onDone) {
		Thread thread = new Thread(() -> {
			T result = networkTask.get();
			SwingUtilities.invokeLater(() -> onDone.accept(result));
		}, "NetworkTask-" + networkTask);
		thread.start();
	}

	public boolean canUseSymbolicLinks() {
		return !System.getProperty("os.name").toLowerCase().contains("win");
	}

	private static class CacheData {

		public CacheData() {
		}

		public CacheData(long downloadTimestamp, String fileName) {
			this.downloadTimestamp = downloadTimestamp;
			this.fileName = fileName;
		}

		private long downloadTimestamp;
		private String fileName;
	}

	private static class CachedApiCall {

		private long downloadTimestamp;
		private String contents;

		public CachedApiCall() {
		}

		public CachedApiCall(long downloadTimestamp, String contents) {
			this.downloadTimestamp = downloadTimestamp;
			this.contents = contents;
		}
	}
}
