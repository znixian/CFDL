/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.cfdl;

import java.util.Comparator;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class SemverSorter implements Comparator<String> {

	private SemverSorter() {
	}

	public static SemverSorter getInstance() {
		return McVersionSorterHolder.INSTANCE;
	}

	@Override
	public int compare(String s1, String s2) {
		if (s1.equals(s2)) {
			return 0;
		}

		String[] parts1 = s1.split("\\.");
		String[] parts2 = s2.split("\\.");

		int len = Math.min(parts1.length, parts2.length);
		for (int i = 0; i < len; i++) {
			try {
				int i1 = Integer.parseInt(parts1[i]);
				int i2 = Integer.parseInt(parts2[i]);
				if (i1 != i2) {
					return i2 - i1;
				}
			} catch (NumberFormatException ex) {
				return -parts1[i].compareTo(parts2[i]);
			}
		}
		return -s1.compareTo(s2);
	}

	private static class McVersionSorterHolder {

		private static final SemverSorter INSTANCE = new SemverSorter();
	}
}
